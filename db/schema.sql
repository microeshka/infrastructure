create extension if not exists "uuid-ossp";

select uuid_generate_v4();

create table users (
    id uuid primary key,
    username varchar(256) not null unique,
    email varchar(256) not null unique,
    full_name varchar(521) not null,
    password varchar(60) not null
);

create table groups (
    id uuid primary key,
    name varchar(256) not null,
    description varchar(256) not null,
    join_id varchar(16),
    created_at timestamp not null default now(),
    creator_id uuid references users(id)
);

create table groups2users (
    group_id uuid references groups(id),
    user_id uuid references users(id),
    primary key (group_id, user_id)
);

create table roles (
    id uuid primary key,
    name varchar(256) not null,
    group_id uuid references groups(id)
);

create table groups2users2roles (
    group_id uuid references groups(id),
    user_id uuid references users(id),
    role_id uuid references roles(id),
    primary key (group_id, user_id, role_id)
);

create extension if not exists ltree;

create table permissions (
    id uuid primary key,
    name ltree not null unique
);

create table roles2permissions (
    role_id uuid references roles(id),
    permission_id uuid references permissions(id),
    primary key (role_id, permission_id)
);

create table subjects (
    id uuid primary key,
    title varchar(256) not null,
    description varchar(256) not null,
    group_id uuid references groups(id) not null,
    created_at timestamp not null default now(),
    creator_id uuid references users(id)
);

create table queues (
    id uuid primary key ,
    title varchar(255) not null,
    description varchar(2048),
    subject_id uuid not null references subjects(id),
    creator_id uuid not null REFERENCES users(id),
    created_at timestamp default now() not null,
    start_date timestamp not null,
    end_date timestamp not null,
    closing_date timestamp
);

create table queue_details (
    id uuid primary key,
    queue_id uuid not null references queues(id),
    user_id uuid not null references users(id),
    sequence_number integer not null default 1,
    passed boolean not null default false,
    turned_at timestamp default now()
);

